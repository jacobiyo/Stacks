package cl.ubb.Stacks;

import static org.junit.Assert.*;

import org.junit.Test;


public class StacksTest {

	@Test
	public void NuevaStackesvacia(){
		/*arrange*/
		Stacks s = new Stacks();
		int resultado;
		
		/*act*/
		resultado = s.Newstack();
		
		/*assert*/
		assertEquals(resultado,0);
	
		}
	@Test 
	public void AgregarUnoStackNoEsVacia(){
		/*arrange*/
		Stacks s= new Stacks();
		int resultado;
		
		/*act*/
		s.Agregar(1);
		resultado=s.Esvacia();
		
		/*assert*/
		assertEquals(resultado,1);
		
	}
	@Test 
	public void AgregarUnoyDosStackNoEsVacia(){
		/*arrange*/
		Stacks s= new Stacks();
		int resultado;
		
		/*act*/
		s.Agregar(1);
		s.Agregar(2);
		resultado=s.Esvacia();
		
		/*assert*/
		assertEquals(resultado,1);
		
	}
	@Test
	public void AgregarUnoYDosTama�oDeStackEs2(){
		/*arrange*/
		Stacks s= new Stacks();
		int resultado;
		/*Act*/
		s.Newstack();
		s.Agregar(1);
		s.Agregar(2);
		resultado=s.Tama�o();
		/*assert*/
		assertEquals(resultado,2);
	}
	@Test
	public void AgregarNumeroUnoHacerPopYObteneruno(){
		/*arrange*/
		Stacks s= new Stacks();
		int resultado;
		/*Act*/
		s.Newstack();
		s.Agregar(1);
		resultado=s.Pop();
		/*assert*/
		assertEquals(resultado,1);
		}
	@Test
	public void AgregarNumeroUnoydosHacerPopYObtenerDOS(){
		/*arrange*/
		Stacks s= new Stacks();
		int resultado;
		/*Act*/
		s.Newstack();
		s.Agregar(1);
		s.Agregar(2);
		resultado=s.Pop();
		/*assert*/
		assertEquals(resultado,2);
		}
	@Test
	public void AgregarNumeroTresYCuatroHacerPopDosVecesYObtenerTresYCuatro()
	{
		/*arrange*/
		Stacks s= new Stacks();
		int resultado,resultado2;
		/*Act*/
		s.Newstack();
		s.Agregar(3);
		s.Agregar(4);
		resultado=s.Pop();
		/*assert*/
		assertEquals(resultado,4);
		resultado2=s.Pop();
		assertEquals(resultado2,3);
		
	}
	@Test
	public void AgregarUnoHacerTopYDevolver1(){
		/*arrange*/
		Stacks s= new Stacks();
		int resultado;
		
		/*act*/
		s.Agregar(1);
		resultado=s.top();
		
		/*assert*/
		assertEquals(resultado,1);
		
		
		
	}
	@Test
	public void AgregarUnoyDosHacerTopYDevolverDOs(){
		/*arrange*/
		Stacks s= new Stacks();
		int resultado;
		
		/*act*/
		s.Agregar(1);
		s.Agregar(2);
		resultado=s.top();
		
		/*assert*/
		assertEquals(resultado,2);
		
		
		
	}

}
